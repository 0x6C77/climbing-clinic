(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

    var ngApp = angular.module("app");
    ngApp.config(['$compileProvider', function ($compileProvider) {
        $compileProvider.debugInfoEnabled(false);

        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|mailto|filesystem|file):/);
    }]);

    ngApp.filter("toArray", function(){
        return function(obj) {
            var result = [];
            angular.forEach(obj, function(val, key) {
                result.push(val);
            });
            return result;
        };
    });

    ngApp.filter('safe', ['$sce', function($sce){
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    }]);

    ngApp.filter('counterValue', function(){
        return function(value){
            var valueInt = parseInt(value);
            if(!isNaN(value) && value >= 0 && value < 10)
                return "0"+ valueInt;
            return valueInt;
        }
    });

    ngApp.filter('clockValue', function(){
        return function(value){
            var valueInt = parseInt(value);
            if(isNaN(value))
                return "00:00";

            var minutes = Math.floor(valueInt / 60);
            var seconds = Math.round(valueInt - minutes * 60);

            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            return minutes + ':' + seconds;
        }
    });

    app.directive('focusInput', function($timeout) {
      return {
        link: function(scope, element, attrs) {
          element.bind('click', function() {
            $timeout(function() {
              element.find('select')[0].focus();
              element.find('input')[0].focus();
            });
          });
        }
      };
    });


    ngApp.controller("ClinicController", ['$scope', '$sce', '$filter', '$interval', function($scope, $sce, $filter, $interval) {

        // Load routines
        var routinesObject = localStorage.getItem('routines');

        $scope.expertRoutines = [
            {
                title: 'Joe Ward',
                byline: 'Let\'s see what you are made of',
                background: 'assets/experts/joe.png',
                hero: 'assets/experts/joe.png',
                description: '<p>Some interesting information</p>',
                set: {
                    hang: 20,
                    rest: 4,
                    reps: 6
                },
                sets: 3,
                rest: 15
            },
            {
                title: 'Chris Pratt',
                byline: 'Ready, set, boogie',
                background: 'assets/experts/chris.png',
                set: {
                    hang: 30,
                    rest: 4,
                    reps: 6
                },
                sets: 30,
                rest: 15
            },
            {
                title: 'Anna Ward',
                background: 'assets/experts/girl.png',
                set: {
                    hang: 20,
                    rest: 4,
                    reps: 6
                },
                sets: 3,
                rest: 15
            }
        ];

        if (routinesObject && JSON.parse(routinesObject)) {
            $scope.routines = JSON.parse(routinesObject);
        } else {
            $scope.routines = [
                {
                    title: 'Default routine',
                    set: {
                        hang: 20,
                        rest: 4,
                        reps: 6
                    },
                    sets: 3,
                    rest: 15
                }
            ];
        }

        var routine;
        var session;

        $scope.details = function(routine) {
            $scope.navigation.pushPage('routine-details.html', {
                data: {
                    routine: routine
                }
            });

            console.log($scope.navigation);
        }

        $scope.start = function(r) {
            routine = r;

            $scope.session = session = {
                start: new Date(),
                reps: 0,
                sets: 0,
                timer: {
                    minutes: 0,
                    seconds: 0,
                    elapsed: {
                        minutes: 0,
                        seconds: 0
                    },
                    remaining: {
                        minutes: 0,
                        seconds: 0
                    }
                }
            };


            if (typeof Media !== 'undefined') {
                if (routine.hangBell) {
                    session.hangBell = new Media('/android_asset/www/assets/' + routine.hangBell + '.mp3');
                }

                if (routine.restBell) {
                    session.restBell = new Media('/android_asset/www/assets/' + routine.restBell + '.mp3');
                }
            }

            if (typeof window.plugins !== 'undefined') {
                window.plugins.insomnia.keepAwake();
            }
            nextSection();

            var duration = 5 + (routine.set.hang * (routine.set.reps * routine.sets)) + (routine.set.rest * (routine.set.reps - 1) * routine.sets) + (routine.rest * (routine.sets - 1));

            session.end = new Date(session.start.getTime() + (1000 * duration));

            update();
            session.interval = $interval(function() {
                update();
            }, 10);

            $scope.navigation.pushPage('timer.html');
        }

        $scope.getDuration = function(routine) {
            return Math.round(((routine.set.hang * (routine.set.reps * routine.sets)) + (routine.set.rest * (routine.set.reps - 1) * routine.sets) + (routine.rest * (routine.sets - 1))) / 60);
        }

        function update() {
            var remaining = (session.current.end - new Date());
            if (remaining <= 0) {
                nextSection();
                remaining = 0;
            }

            // Update timer
            var milliseconds = parseInt((remaining%1000)/10)
                , seconds = parseInt((remaining/1000)%60)
                , minutes = parseInt((remaining/(1000*60))%60)
                , hours = parseInt((remaining/(1000*60*60))%24);

            session.timer.minutes = (minutes < 10) ? "0" + minutes : minutes;
            session.timer.seconds = (seconds < 10) ? "0" + seconds : seconds;
            session.timer.milliseconds = milliseconds;

            // Calculate remaining
            var remaining = (session.end - new Date()) / 1000;
            if (remaining < 0) {
                remaining = 0;
            }
            session.timer.remaining.minutes = Math.floor(remaining / 60);
            session.timer.remaining.seconds = Math.round(remaining - session.timer.remaining.minutes * 60);

            // Calculate elapsed
            var elapsed = (new Date() - session.start) / 1000;
            session.timer.elapsed.minutes = Math.floor(elapsed / 60);
            session.timer.elapsed.seconds = Math.round(elapsed - session.timer.elapsed.minutes * 60);
        }

        function nextSection() {
            if (!session.current) {
                session.current = {};
                session.current.type = 'start';
                session.current.text = 'get ready';
                session.current.end = new Date();
                session.current.end.setSeconds(session.current.end.getSeconds() + 5);
            } else if (session.current.type == 'start' || session.current.type == 'rest' || session.current.type == 'setrest') {
                session.current = {};
                session.current.type = 'hang';
                session.current.text = 'hang';
                session.current.end = new Date();
                session.current.end.setSeconds(session.current.end.getSeconds() + routine.set.hang);

                // Hang sound            
                if (typeof session.hangBell !== 'undefined') {
                    session.hangBell.stop();
                    session.hangBell.play();
                }
            } else if (session.current.type == 'hang') {
                session.reps++;

                // Rest sound            
                if (typeof session.restBell !== 'undefined') {
                    session.restBell.stop();
                    session.restBell.play();
                }

                if (session.reps >= routine.set.reps) {
                    session.reps = 0;
                    session.sets++;

                    if (session.sets >= routine.sets) {
                        $scope.stop(true);
                        return;
                    }

                    session.current = {};
                    session.current.type = 'setrest';
                    session.current.text = 'rest';
                    session.current.end = new Date();
                    session.current.end.setSeconds(session.current.end.getSeconds() + routine.rest);

                } else {
                    session.current = {};
                    session.current.type = 'rest';
                    session.current.text = 'rest';
                    session.current.end = new Date();
                    session.current.end.setSeconds(session.current.end.getSeconds() + routine.set.rest);
                }
            }

            $scope.stop = function(completed) {
                if (completed) {
                    session.finished = true;
                    session.current.type = 'finished';
                }
                $interval.cancel(session.interval);
            }

            update();
        }


        $scope.newRoutine = function() {
            $scope.editingID = -1;
            $scope.editing = {
                title: '',
                set: {
                    hang: 0,
                    rest: 0,
                    reps: 0
                },
                sets: 0,
                rest: 0
            };
            $scope.navigation.pushPage('routine-edit.html');
        }

        $scope.editRoutine = function($event, id) {
            $event.stopPropagation();
            $scope.editingID = id;
            $scope.editing = JSON.parse(JSON.stringify($scope.routines[id]));
            $scope.navigation.pushPage('routine-edit.html');
        }

        $scope.saveRoutine = function() {
            if (!$scope.editing.title) {
                ons.notification.alert({
                    message: 'You need to give your routine a title!'
                });
                return;
            }

            if ($scope.editingID >= 0) {
                $scope.routines[$scope.editingID] = $scope.editing;
            } else {
                $scope.routines.push($scope.editing);
            }

            // Save
            localStorage.setItem('routines', JSON.stringify($scope.routines));

            $scope.navigation.popPage();
        }

        $scope.deleteRoutine = function() {
            ons.notification.confirm({
                message: 'Are you sure you want to delete this routine?',
                callback: function(idx) {
                    switch (idx) {
                        case 1:
                           $scope.routines.splice($scope.editingID, 1);
                           $scope.$apply(); // Update UI

                            // Save
                            localStorage.setItem('routines', JSON.stringify($scope.routines));

                            $scope.navigation.popPage();     
                        break;
                    }
                }
            });
        }

        $scope.saveExpertRoutine = function(routine) {
            if (!$scope.routines.find(o => o.title === routine.title)) {
                $scope.routines.push(routine);

                // Save
                localStorage.setItem('routines', JSON.stringify($scope.routines));
            }


            alert('Routine added');
        }

        $scope.playSound = function(sound) {
            if (typeof Media !== 'undefined') {
                var m = new Media('/android_asset/www/assets/' + sound + '.mp3');
                m.play();
            }
        }

        var timeInput = function() {
            this.show = function(a, b) {
                this.a = a;
                this.b = b;

                var original;
                if (this.b) {
                    original = $scope.editing[this.a][this.b];
                } else {
                    original = $scope.editing[this.a];
                }

                this.minutes = Math.floor(original / 60);
                this.seconds = Math.round(original - this.minutes * 60);

                $scope.timeInputModal.show();
            }

            this.hide = function() {
                $scope.timeInputModal.hide();
            }

            this.minutesUp = function() {
                if (this.minutes < 59) {
                    this.minutes++;
                }
                this.updateRoutine();
            }

            this.minutesDown = function() {
                if (this.minutes > 0) {
                    this.minutes--;
                }
                this.updateRoutine();
            }

            this.secondsUp = function() {
                if (this.seconds < 59) {
                    this.seconds++;
                } else if (this.minutes < 59) {
                    this.seconds = 0;
                    this.minutesUp();
                }
                this.updateRoutine();
            }

            this.secondsDown = function() {
                if (this.seconds > 0) {
                    this.seconds--;
                } else if (this.minutes > 0) {
                    this.seconds = 59;
                    this.minutesDown();
                }
                this.updateRoutine();
            }

            this.updateRoutine = function() {
                if (this.b) {
                    $scope.editing[this.a][this.b] = this.minutes * 60 + this.seconds;
                } else {
                    $scope.editing[this.a] = this.minutes * 60 + this.seconds;
                }
            }
        }

        $scope.timeInput = new timeInput();

    }]);

})();