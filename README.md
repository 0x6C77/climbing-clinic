# Climbing clinic
A hangboard timer to help climbers accurately train on a hangboard.

| Landing page | Routine selection |
| ------ | ------ |
| ![homepage](https://gitlab.com/0x6C77/climbing-clinic/raw/master/static/home.png) | ![homepage](https://gitlab.com/0x6C77/climbing-clinic/raw/master/static/menu.png) | 


## Compile SCSS
```bash
npm install -g node-sass
./sass.sh
```